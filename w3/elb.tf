# Add missing arguments according to terraform/aws documentation.
# Docs: https://www.terraform.io/docs/providers/aws/r/elb.html
resource "aws_elb" "w3" {
  name = "w3-elb"
  security_groups = [ "${aws_security_group.w3-security-group.id}" ]
  connection_draining = true

  listener {
    instance_port = 
    instance_protocol = ""
    lb_port = 
    lb_protocol = ""
  }

  # Configure ELB health checks here
  health_check {
    target = "HTTP:xx/"
    healthy_threshold = 
    unhealthy_threshold = 
    timeout = 
    interval = 
  }

  # Keep these arguments as is:
  subnets = [ "${var.subnet_id}" ]
  internal = true
}
