# Uncomment the resources below and add the required arguments.

resource "aws_security_group" "ws1_s12_sg" {
  # 1. Define logical names (identifiers) for resource.
  #    Eg: resource "type" "resource_logical_name" {}
  #    Docs: https://www.terraform.io/docs/providers/aws/r/security_group.html

  # 2. Set the name of your security group below in format "studentx-sg"
  name = "workshop1_s12_sg"

  description = "Student12 security group."
  vpc_id = "vpc-7c528404"
}

# To reference attributes of resources use syntax TYPE.NAME.ATTRIBUTE
#   for example, in order to create rule in specific secrurity group you will have to
#   refer security group by its name :
#     security_group_id = "${aws_security_group.mysecuritygroup.id}"
#
# Reference: https://www.terraform.io/docs/configuration/interpolation.html


resource "aws_security_group_rule" "ssh_ingress_access" {
  # 1. Add required arguments to open ingress(incoming) traffic to TCP port 22 - we'll use it later to ssh into the instance.
  # 2. Add argument to reference Security Group resource.
  # Docs: https://www.terraform.io/docs/providers/aws/r/security_group_rule.html
  
  # ...
  type = "ingress"
  from_port = 22
  to_port = 22
  protocol = "tcp"
  security_group_id = "${aws_security_group.ws1_s12_sg.id}"
  cidr_blocks = [ "0.0.0.0/0" ] 
}

resource "aws_security_group_rule" "egress_access" {
  # 1. Add required arguments to open outgoing traffic to all ports (0-65535) 
  # 2. Add argument to reference Security Group resource.
  # Docs: https://www.terraform.io/docs/providers/aws/r/security_group_rule.html
  
  # ...
  type = "egress"
  from_port = 0
  to_port = 65535
  protocol = "-1"
  security_group_id = "${aws_security_group.ws1_s12_sg.id}"
  cidr_blocks = [ "0.0.0.0/0" ]
}

resource "aws_instance" "the_brain" {
  # 1. Add resource name.
  # 2. Specify VPC subnet ID
  # 3. Specify EC2 instance type.
  # 4. Specify Security group for this instance (use one that we create above).
  # Docs: https://www.terraform.io/docs/providers/aws/r/instance.html

  subnet_id = "subnet-a3074be8"

  instance_type = "t2.micro"
  vpc_security_group_ids = [ "${aws_security_group.ws1_s12_sg.id}"]
  associate_public_ip_address = true
   user_data = "${file("../shared/user-data.txt")}"
  tags {
    Name = "w1-student12_the_brain"
  }
  
  # Keep these arguments as is, if in N. Virginia: otherwise replace with the ami for Amazon Linux AMI     2018.03.0 (HVM)/availability_zone for your region

  ami = "ami-08d489468314a58df"
  availability_zone = "us-west-2a"

 # example for Ireland 	
 # ami = "ami-028188d9b49b32a80"
 # availability_zone = "eu-west-1c"

}
