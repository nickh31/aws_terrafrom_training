# Specify missing arguments according to documentation:
# https://www.terraform.io/docs/providers/aws/r/launch_configuration.html
resource "aws_launch_configuration" "pinky_plan" {
  security_groups = ["${aws_security_group.w2_security_group_student12.id}"]
  user_data = "${file("../shared/user-data.txt")}"

  # Keep below arguments for the default region - you will have to change the ami     for another region

  lifecycle { create_before_destroy = true }
  instance_type = "t2.micro"
  image_id = "ami-08d489468314a58df"
}

# Specify missing arguments according to documentation:
# https://www.terraform.io/docs/providers/aws/r/autoscaling_group.html
resource "aws_autoscaling_group" "brain_world" {
  name = "brain_world"
  min_size = 1
  max_size = 3
  launch_configuration = "${aws_launch_configuration.pinky_plan.name}"
  default_cooldown = 60

  # Keep below arguments
  availability_zones = [ "${var.availability_zone_id}" ]
  vpc_zone_identifier = [ "${var.subnet_id}" ]

  tag {
    key = "Name"
  # change the studentx to your student id
    value = "terraform_workshop_student12"
    propagate_at_launch = true
  }

  lifecycle { create_before_destroy = true }
}

# Uncomment and specify arguments according to documentation and workshop guide:
# https://www.terraform.io/docs/providers/aws/r/autoscaling_policy.html
resource "aws_autoscaling_policy" "autoscale_group_policy_up_x1" {
  name = "autoscale_group_policy_up_x1"
  scaling_adjustment = 1
  adjustment_type = "ChangeInCapacity"
  cooldown = 60
  autoscaling_group_name = "${aws_autoscaling_group.brain_world.name}"
}

resource "aws_autoscaling_policy" "autoscale_group_policy_down_x1" {
  name = "autoscale_group_policy_down_x1"
  scaling_adjustment = -1
  adjustment_type = "ChangeInCapacity"
  cooldown = 60
  autoscaling_group_name = "${aws_autoscaling_group.brain_world.name}"
}

